from django import forms
from . models import Team
from django.contrib.auth import get_user_model
User = get_user_model()

class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = ('name', 'lead', 'members')

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].label = 'Team Name'
        self.fields['lead'].label = 'Team Lead'
        self.fields['lead'].queryset = User.objects.filter(company=user.company)
        self.fields['members'] = forms.ModelMultipleChoiceField(
            label='Team Members:',
            widget=forms.CheckboxSelectMultiple,
            queryset=User.objects.filter(company=user.company)
        )

        if user.team and (not user.is_admin) :
            self.fields['lead'].widget.attrs['disabled'] = True
