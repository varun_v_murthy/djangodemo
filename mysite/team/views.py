from django.urls import reverse_lazy
from braces import views as auth_views
from django.views import generic #TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from django.core import exceptions
from . import models
from .forms import TeamForm
from django.contrib.auth import get_user_model
User = get_user_model()

#Team CRUD Classes
class TeamCreateView(auth_views.UserPassesTestMixin, auth_views.LoginRequiredMixin, generic.CreateView):
    model = models.Team
    form_class = TeamForm

    def test_func(self, user):
        if user.is_admin:
            return True
        else:
            raise exceptions.PermissionDenied

    def get_form_kwargs(self, **kwargs):
        form_kwargs = super().get_form_kwargs(**kwargs)
        form_kwargs["user"] = self.request.user
        return form_kwargs

    def form_valid(self, form):
        self.team = form.save(commit=False)
        self.team.company = self.request.user.company
        self.team.save()
        return super().form_valid(form)


class TeamListView(auth_views.UserPassesTestMixin, auth_views.LoginRequiredMixin, generic.ListView):
    model = models.Team
    context_object_name = 'teams'

    def get_queryset(self):
        return models.Team.objects.filter(company=self.request.user.company)

    def test_func(self, user):
        if user.is_admin:
            return True
        else:
            raise exceptions.PermissionDenied


class TeamDetailView(auth_views.LoginRequiredMixin,generic.DetailView):
    context_object_name = 'team_details'
    model = models.Team


class TeamUpdateView(auth_views.UserPassesTestMixin, auth_views.LoginRequiredMixin, generic.UpdateView):
    model = models.Team
    form_class = TeamForm

    def get_form_kwargs(self, **kwargs):
        form_kwargs = super().get_form_kwargs(**kwargs)
        form_kwargs["user"] = self.request.user
        return form_kwargs

    def test_func(self, user):
        if user.is_admin:
            return True
        elif self.get_object().lead == user:
            return True
        else:
            raise exceptions.PermissionDenied



class TeamDeleteView(auth_views.UserPassesTestMixin, auth_views.LoginRequiredMixin, generic.DeleteView):
    model = models.Team
    success_url = reverse_lazy('team:team_list')

    def test_func(self, user):
        if user.is_admin:
            return True
        else:
            raise exceptions.PermissionDenied
