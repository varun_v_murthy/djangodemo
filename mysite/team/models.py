from django.db import models
from django.urls import reverse
from registry.models import Company, EmployeeUser

class Team(models.Model):
    name = models.CharField(max_length=250, blank=False)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='teams')
    lead = models.OneToOneField(EmployeeUser, on_delete=models.CASCADE, related_name='team')
    members = models.ManyToManyField(EmployeeUser, related_name='team_member')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('team:team_list')
