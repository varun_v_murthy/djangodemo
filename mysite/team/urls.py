from django.urls import path, re_path
from . import views

app_name = 'team'

urlpatterns = [
    re_path(r'^all$', views.TeamListView.as_view(), name='team_list'),
    re_path(r'^add$', views.TeamCreateView.as_view(), name='team_add'),
    re_path(r'^(?P<pk>\d+)/$', views.TeamDetailView.as_view(), name='team_detail'),
    re_path(r'^edit/(?P<pk>\d+)', views.TeamUpdateView.as_view(), name='team_edit'),
    re_path(r'^delete/(?P<pk>\d+)', views.TeamDeleteView.as_view(), name='team_delete'),
]
