# Generated by Django 2.1.4 on 2018-12-06 09:05

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('registry', '0003_auto_20181205_1331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employeeuser',
            name='is_admin',
            field=models.BooleanField(default=False, verbose_name='Company Admin'),
        ),
        migrations.AlterField(
            model_name='team',
            name='lead',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='team', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='team',
            name='member',
            field=models.ManyToManyField(related_name='team_member', to=settings.AUTH_USER_MODEL),
        ),
    ]
