# Generated by Django 2.1.4 on 2018-12-06 09:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registry', '0004_auto_20181206_1435'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employeeuser',
            name='email',
            field=models.EmailField(max_length=254, unique=True, verbose_name='email address'),
        ),
    ]
