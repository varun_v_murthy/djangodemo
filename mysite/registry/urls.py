from django.urls import path, re_path
from . import views

app_name = 'registery'

urlpatterns = [
    path('companies/',views.CompanyListView.as_view(), name='company_list'),
    re_path(r'^company/(?P<pk>\d+)$', views.CompanyDetailView.as_view(), name='company_detail'),
    re_path(r'^company/add$', views.CompanyCreateView.as_view(), name='company_add'),
    re_path(r'^company/update/(?P<pk>\d+)$', views.CompanyUpdateView.as_view(), name='company_update'),
    re_path(r'^company/delete/(?P<pk>\d+)$', views.CompanyDeleteView.as_view(), name='company_delete'),
    re_path(r'^employee/add$', views.EmployeeUserCreateView.as_view(), name='employee_add'),
    re_path(r'^profile/edit/(?P<pk>\d+)$', views.EmployeeUserUpdateView.as_view(), name='employee_edit'),
    re_path(r'^employees/$', views.EmployeeUserListView.as_view(), name='employee_list'),
    re_path(r'^employee/delete/(?P<pk>\d+)$', views.EmployeeUserDeleteView.as_view(), name='employee_delete'),
]
