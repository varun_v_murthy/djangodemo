from django.db import models
from django.urls import reverse
from django.contrib.auth.models import AbstractUser


class Company(models.Model):
    name = models.CharField(max_length=100, unique=True, blank=False, null=False)
    address = models.TextField(blank=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('registry:company_detail', kwargs={'pk':self.pk})


class EmployeeUser(AbstractUser):
    is_admin = models.BooleanField('Company Admin', default=False)
    address = models.TextField(blank=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='employees', blank=False, null=True)

    def __init__(self, *args, **kwargs):
        self._meta.get_field('email').blank = False
        self._meta.get_field('email')._unique = True
        super().__init__(*args, **kwargs)

    def __str__(self):
        return self.email + " - " + self.get_full_name()

    def get_absolute_url(self):
        return reverse('registry:employee_list')
