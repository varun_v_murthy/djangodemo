from django.urls import reverse_lazy
from braces import views as auth_views
from django.views import generic #TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView
from django.core import exceptions
from django.db import transaction
from . import models
from .forms import CompanyForm
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
User = get_user_model()


# Company CRUD Classed
class CompanyListView(generic.ListView):
    context_object_name = 'companys'
    model = models.Company


class CompanyDetailView(auth_views.SuperuserRequiredMixin, generic.DetailView):
    context_object_name = 'company_details'
    model = models.Company
    template_name = 'registry/company_detail.html'


class CompanyCreateView(auth_views.SuperuserRequiredMixin, generic.CreateView):
    model = models.Company
    form_class = CompanyForm

    def form_valid(self, form):
        self.email = form.cleaned_data['email']

        with transaction.atomic():
            self.company = form.save()

            my_password = User.objects.make_random_password(length=10)
            user = User.objects.create_user(username=self.email, email=self.email, password=my_password)
            if user:
                user.is_admin = True
                user.company = self.company
                send_mail(
                    subject="Welcome to MySite",
                    message=f"Here are the details \n Login:{user.email} \n Password: {my_password}",
                    from_email='admin@mysite.com',
                    recipient_list=[user.email,],
                    fail_silently=False
                )
                user.save()

        return super().form_valid(form)


class CompanyUpdateView(auth_views.SuperuserRequiredMixin, generic.UpdateView):
    model = models.Company
    fields = ('name', 'address')


class CompanyDeleteView(auth_views.SuperuserRequiredMixin, generic.DeleteView):
    model = models.Company
    success_url = reverse_lazy('registry:company_list')


#Employee CRUD Classes
class EmployeeUserCreateView(auth_views.UserPassesTestMixin, auth_views.LoginRequiredMixin, generic.CreateView):
    model = models.EmployeeUser
    fields = ('email', 'first_name', 'last_name', 'is_admin')

    def test_func(self, user):
        if user.is_admin:
            return True
        else:
            raise exceptions.PermissionDenied

    def form_valid(self, form):
        self.emp = form.save(commit=False)
        self.emp.username = self.emp.email
        self.emp.company = self.request.user.company
        my_password = User.objects.make_random_password(length=10)
        send_mail(
            subject="Welcome to MySite",
            message=f"Here are the details \n Login:{self.emp.email} \n Password: {my_password}",
            from_email='admin@mysite.com',
            recipient_list=[self.emp.email,],
            fail_silently=False
        )
        self.emp.set_password(my_password)
        self.emp.save()
        return super().form_valid(form)


class EmployeeUserUpdateView(auth_views.UserPassesTestMixin, auth_views.LoginRequiredMixin, generic.UpdateView):
    model = models.EmployeeUser
    fields = ('username', 'first_name', 'last_name', 'email', 'address')

    def test_func(self, user):
        if str(user.pk) == self.kwargs['pk']:
            return True
        else:
            raise exceptions.PermissionDenied


class EmployeeUserListView(auth_views.UserPassesTestMixin, auth_views.LoginRequiredMixin, generic.ListView):
    context_object_name = 'employeeusers'
    model = models.EmployeeUser

    def get_queryset(self):
        return User.objects.filter(company=self.request.user.company)

    def test_func(self, user):
        if user.is_admin:
            return True
        else:
            raise exceptions.PermissionDenied


class EmployeeUserDeleteView(auth_views.UserPassesTestMixin, auth_views.LoginRequiredMixin, generic.DeleteView):
    model = models.EmployeeUser
    success_url = reverse_lazy('registry:employee_list')

    def test_func(self, user):
        if user.is_admin:
            return True
        else:
            raise exceptions.PermissionDenied
