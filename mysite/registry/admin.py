from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import EmployeeCreationForm, EmployeeChangeForm
from .models import Company, EmployeeUser


class EmployeeUserAdmin(UserAdmin):
    add_form = EmployeeCreationForm
    form = EmployeeChangeForm
    model = EmployeeUser
    list_display = ('username', 'email', 'company', 'is_admin')

admin.site.register(Company)
admin.site.register(EmployeeUser, EmployeeUserAdmin)
