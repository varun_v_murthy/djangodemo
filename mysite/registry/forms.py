from django import forms
from django.contrib.auth.forms import UserCreationForm,UserChangeForm
from .models import EmployeeUser, Company
from django.contrib.auth import get_user_model
User = get_user_model()


class EmployeeCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = EmployeeUser
        fields = ['username', 'email']


class EmployeeChangeForm(UserChangeForm):

    class Meta(UserChangeForm):
        model = EmployeeUser
        fields = ['username', 'email']


class CompanyForm(forms.ModelForm):
    email = forms.EmailField(required = True)

    class Meta:
        model = Company
        fields = ('name', 'address', 'email')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].label = 'Company Name'
        self.fields['address'].label = 'Company Address'
        self.fields['email'].label = "Company's Admin Email"
