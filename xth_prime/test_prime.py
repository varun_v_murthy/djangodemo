# import unittest module and prime to test
import unittest
from prime import xth_prime

# creating a test class
class TestPrime(unittest.TestCase):

    # defining 1st test by checking if 2nd prime is given as 3 [2, 3]
    def test_2nd_prime(self):
        res = xth_prime(2)
        self.assertEqual(res,3)

    # defining 2nd test by checking if 5th prime is given as 11 [2, 3, 5, 7, 11]
    def test_5th_prime(self):
        res = xth_prime(5)
        self.assertEqual(res, 11)

    # defining 3rd test by checking if 6th prime is given as 11 [2, 3, 5, 7, 11, 13]
    def test_6th_prime(self):
        res = xth_prime(6)
        self.assertEqual(res,13)

    # defining 4th test by giving non-interger value as input
    def test_non_interger(self):
        res = xth_prime('1')
        self.assertEqual(res,None)

if __name__ == '__main__':
    unittest.main()
