def xth_prime(x):

    if not isinstance(x,int):
        print ('NonIntegerError')
        return

    # initial list of prime number
    prime_list = [2]
    # second prime number to start checking prime
    num = 3

    # loop to keep generating primes until we get xth prime
    while len(prime_list) < x:

        # loop to check if 'num' is prime
        for p in prime_list:
            # if the reminder is 0 then 'num' is not prime
            if num % p == 0:
                # break to go out of loop to check next 'num'
                break

        # if it is a prime, then add it to the list after completing for loop
        else:
            # append 'num' to prime list
            prime_list.append(num)

        # optimization to check only add numbers
        num += 2

    # return the last prime number generated
    return prime_list[-1]

if __name__ == '__main__':

    # loop till you get a valid input
    while(True):

        # try block to catch non number inputs
        try:
            # getting an input
            x=int(input('Enter the position number of the prime you want to find: '))

            # basic validation to see if the input is valid
            if x < 1:
                # showing error message
                print ('Position number should be positive integer number!')
                # going to loop
                continue
            # printing the output
            print(f'Prime number of {x} position : {xth_prime(x)}')
            break

        # if non number is given as input
        except ValueError:
            # show error
            print('Please Enter A Number!')
            # go back to loop to ask a number
            continue
